#!/usr/bin/perl
use config;
use strict;
use warnings;
use Net::DNS::Nameserver;

sub reply_handler {
        my ($qname, $qclass, $qtype, $peerhost,$query,$conn) = @_;
        my ($rcode, @ans, @auth, @add);
        my $subsub;
        my $zfr;
        my @ZONES;
        my $SourceServer = $peerhost;

        open(ZONE, "< /var/www/cgi-bin/zonefile") or die "$!\n";
        @ZONES = <ZONE>;
        close(ZONE);

        foreach $zfr (@ZONES) {         # zfr is "zone file record"

        my ($zfr_d,$zfr_t,$zfr_i) = split(/ /, $zfr);

        if ($qtype eq $zfr_t && $qname =~ m/$zfr_d$/ ) {

                if($qname =~ m/^([a-zA-Z0-9]+)\.$zfr_d$/) { $subsub = $1; }
                if($subsub) {
                        open(LOOKUP, "> ".$config::docroot."/".$subsub) or die "$!\n";
                        print LOOKUP "$SourceServer\n";
                        close(LOOKUP);
                        }
                my ($ttl, $rdata) = ($config::ttl, $zfr_i);
                my $rr = new Net::DNS::RR("$qname $ttl $qclass $qtype $rdata");
                push @ans, $rr;
                $rcode = "NOERROR";
        }
        elsif( $qname eq $zfr_d ) {
                $rcode = "NOERROR";
        }
        else{
                $rcode = "NXDOMAIN";
        }

 return ($rcode, \@ans, \@auth, \@add, { aa => 1 });
 } #END FOR LOOP
} # END SUB-ROUTINE reply_handler


my $named = new Net::DNS::Nameserver(
    LocalPort    => 53,
    ReplyHandler => \&reply_handler,
    Verbose      => 0
) || die "couldn't create nameserver object\n";


$named->main_loop;