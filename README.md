#Perl Resolver
This project is a fork of resolver-tester modified to use perl and SSI rather than python and jquery. You can test it at http://resolver.supervene.com

#How it works
##Components
1) Web Server

2) Net::DNS::Nameserver module from CPAN.

##DNS Lookup Methods
1) Look up image with user-unique domain names when the user loads the page

2) Give the user links to web pages with the user-unique domain names - work in progress

3) Give the user commands that they can run on their computer to run the DNS queries from the command line - work in progress

#Set Up
##Requirements
* Perl
* Net::DNS::Nameserver
* web server supporting SSI

##Getting Started
1)Get code and requisites

	git clone https://bitbucket.org/p3rlphr33k/perl-resolver.git
	cd perl-resolver
	on redhat/centos: sudo yum install 'perl(Net::DNS::Nameserver)'
	on ubuntu based systems: 
		sudo aptitude install libnet-dns-perl
		sudo apt-get install libnet-dns-perl

2) Move the contents of `web/` to your web server document root

3) Move the rest of the contents to your web server cgi-bin

4) make sure scripts have correct permssions: 
	
	chmod 0775 server.pl
	chmod 0775 watchdog.pl - watchdog is optional but helpful

5) Edit config.pm to reflect your html directory and TTL - more consolidating will come soon

6) Launch DNS server in tmux, screen session with:

	sudo perl server.pl

or launch in current session background with nohup: 
	
	sudo perl server.pl nohup &

## optional component watchdog.pl

watchdog will run in your crontab every 5 minutes to check if server.pl is running. If not it will run the nohup command. Watchdog will also cleanup old requests from the web directory on the hour.

This script is optional but recommended to prevent request dir temp files from growing.

I had this built into the script but removed it to keep the latency down on DNS responses. I will publish that copy as: cronless-server.pl

If you decide to use crontab you can import directly by cd into cgi-bin and running:
	crontab crontab.txt

Check path in crontab.txt prior to import.


######## OTHER NOTES #########

This is a mashup of a project. I will consolidate the config, htaccess, and zonefile soon. until then its a big mess. I will also add docs to help configure your existing DNS for this utility.