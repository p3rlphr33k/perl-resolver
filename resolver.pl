#!/usr/bin/perl
use strict;
use config;

my $in = $ENV{'QUERY_STRING'};

my ($k,$v) = split(/=/, $in);

open(RESOLVE , "< $config::docroot/$v") or die "$!\n";
my @IPS = <RESOLVE>;
close(RESOLVE);

print "Content-Type: text/html\n\n";

my $theIP = pop(@IPS);

$theIP =~ s/\n//g;

print qq~
var Resolver = "$theIP";
~;

