#!/usr/bin/perl

use strict;

my $exists = `ps a | grep '[s]erver.pl'`;

if (!$exists) {
    `perl /var/www/cgi-bin/server.pl nohup &`;
}
